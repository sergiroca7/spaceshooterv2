﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoS : Bullet
{

    public int maxAmmo = 2;
    Cartridge cartridge;
    public int timeToExplode = 1;
    private float currentTime = 0f;

    void Awake()
    {
        cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[1];
    }

    protected new virtual void Update()
    {
        base.Update();
        if(shooting)
        {
            currentTime += Time.deltaTime;
            if(currentTime > timeToExplode)
            {
                ShotMiniBullets();
            }
        }
        else
        {
            currentTime = 0f;
        }
    }

    void ShotMiniBullets()
    {
        Debug.Log("Shot mini bulletsS");
        float z = transform.rotation.eulerAngles.z;

        cartridge.GetBullet().Shot(transform.position, (-45 + z) + (45 + z));
        cartridge.GetBullet().Shot(transform.position, (45 + z) - (45  - z));

        Reset();
    }
}