﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public Propeller propeller;
    private BoxCollider2D myCollider;
    public AudioSource audiop;
    public GameObject graphics;
    public ParticleSystem explotion;


    // Update is called once per frame

    private void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();
    }
    void Update()
    {
        transform.Translate(axis * speed * Time.deltaTime);

        if(transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if(transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }

        if(transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if(transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }

        if(axis.y > 0)
        {
            propeller.RedFire();
        }
        else if(axis.y < 0)
        {
            propeller.BlueFire();
        }
        else
        {
            propeller.Stop();
        }
    }

    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Meteor")
        {
            Explode();
        }
    }

    private void Explode()
    {
        graphics.SetActive(false);

        myCollider.enabled = false;

        explotion.Play();

        audiop.Play();

        Invoke("Reset", 2);
    }
    private void Reset()
    {
        graphics.SetActive(true);
        myCollider.enabled = true;
    }
}