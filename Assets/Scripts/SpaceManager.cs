﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpaceManager : MonoBehaviour {

    public bool Pausa = false;
    public GameObject pauseMenu;
    public static SpaceManager instance;
    private int highscore;
    public Text highscoreText;

	// Use this for initialization
	void Start () {
        instance = this;
        highscore = 0;
        highscoreText.text = highscore.ToString("D5");


	}
	public void AddHighscore(int value)
    {
        highscore += value;
        highscoreText.text = highscore.ToString("D5");

    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown (KeyCode.Escape))
        {
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }
	}

    public void Resume()
    {
        Debug.Log("RESUME");
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }


    public void MainMenu()
    {
        Debug.Log("Main Menu");
        SceneManager.LoadScene("MainMenu");
    }

}
